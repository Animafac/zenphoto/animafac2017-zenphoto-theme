<?php
// force UTF-8 Ø

if (!defined('WEBPATH')) {
    die();
}
global $_zp_current_image;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="<?php echo LOCAL_CHARSET; ?>">
        <?php zp_apply_filter('theme_head'); ?>
        <?php printHeadTitle(' - '); ?>
        <link rel="stylesheet" href="<?php echo pathurlencode($zenCSS); ?>" type="text/css" />
        <link rel="stylesheet" href="<?php echo pathurlencode(dirname(dirname($zenCSS))); ?>/common.css" type="text/css" />
        <?php if (zp_has_filter('theme_head', 'colorbox::css')) { ?>
            <script type="text/javascript">
                // <!-- <![CDATA[
                $(document).ready(function() {
                    $(".colorbox").colorbox({
                        inline: true,
                        href: "#imagemetadata",
                        close: '<?php echo gettext("close"); ?>'
                    });
                    $(".fullimage").colorbox({
                        maxWidth: "98%",
                        maxHeight: "98%",
                        photo: true,
                        close: '<?php echo gettext("close"); ?>',
                            onComplete: function(){
                                $(window).resize(resizeColorBoxImage);
                            }
                    });
                });
                // ]]> -->
            </script>
        <?php } ?>
        <?php
        if (class_exists('RSS')) {
            printRSSHeaderLink('Gallery', gettext('Gallery RSS'));
        }
        ?>
        <?php include_once __DIR__.'/inc/head.php'; ?>
        <meta name="twitter:title" content="<?php echo getHeadTitle(' - '); ?>" />
        <meta name="twitter:site" content="@animafac" />
        <meta name="twitter:image" content="<?php echo html_encode('https://'.$_SERVER['HTTP_HOST'].$_zp_current_image->getCustomImage(1110, null, null, null, null, null, null, true)); ?>" />
        <meta name="twitter:description" content="<?php printImageDesc(); ?>" />
        <meta property="og:title" content="<?php echo getHeadTitle(' - '); ?>" />
        <meta property="og:url" content="<?php echo html_encode('http://'.$_SERVER['HTTP_HOST'].getImageURL()); ?>" />
        <meta property="og:locale" content="fr_FR" />
        <meta property="og:type" content="website" />
        <meta property="og:description" content="<?php printImageDesc(); ?>" />
        <?php if (isImagePhoto()) : ?>
            <meta name="twitter:card" content="summary_large_image" />
            <meta property="og:image" content="<?php echo str_replace(' ', '%20', html_encode('http://'.$_SERVER['HTTP_HOST'].getFullImageURL())); ?>" />
        <?php else : ?>
            <meta name="twitter:card" content="player" />
            <meta name="twitter:player" content="<?php echo html_encode('https://'.$_SERVER['HTTP_HOST'].getImageURL().'?iframe=1&twitter=1'); ?>" />
            <meta name="twitter:player:stream" content="<?php echo html_encode('https://'.$_SERVER['HTTP_HOST'].getFullImageURL()); ?>" />
            <meta name="twitter:player:stream:content_type" content="video/mp4" />
            <meta name="twitter:image" content="<?php echo str_replace(' ', '%20', html_encode('http://'.$_SERVER['HTTP_HOST'].$_zp_current_image->getCustomImage(1110, null, null, null, null, null, null, true))); ?>" />
            <meta property="og:image" content="<?php echo str_replace(' ', '%20', html_encode('http://'.$_SERVER['HTTP_HOST'].$_zp_current_image->getCustomImage(1110, null, null, null, null, null, null, true))); ?>" />
            <meta property="og:video" content="<?php echo str_replace(' ', '%20', html_encode('https://'.$_SERVER['HTTP_HOST'].getFullImageURL())); ?>" />
            <meta property="og:video:secure_url" content="<?php echo str_replace(' ', '%20', html_encode('https://'.$_SERVER['HTTP_HOST'].getFullImageURL())); ?>" />
            <meta property="og:video:type" content="video/mp4" />
            <meta property="og:video:width" content="1280" />
            <meta property="og:video:height" content="720" />
            <meta name="twitter:player:width" content="900" />
            <meta name="twitter:player:height" content="500" />
        <?php endif; ?>
        <link rel="canonical" href="<?php echo html_encode('http://'.$_SERVER['HTTP_HOST'].getImageURL()); ?>" />
    </head>
    <body class="single <?php
    if (isset($_GET['iframe'])) {
        echo ' iframe';
    }
    if (isset($_GET['twitter'])) {
        echo ' twitter';
    }
        ?>">
        <?php zp_apply_filter('theme_body_open'); ?>
        <div class="wrap container">
            <a class="getback" href="<?php echo FULLWEBPATH; ?>">◀ Retourner à la galerie</a>
            <div class="main">
                <div class="header-fiche"><h1><img alt="<?php printAlbumTitle(); ?>" src="<?php echo pathurlencode(dirname(dirname($zenCSS))); ?>/images/Logo%20Engageons%202017-noir.png" /></h1></div>
                <div id="main">
                    <div class="fiche-content-full">
                        <?php
                            if (isImagePhoto()) {
                                $fullimage = getFullImageURL();
                            } else {
                                $fullimage = null;
                            }
                            echo '<h2>'.getImageTitle().'</h2>';
                            if (isImagePhoto()) {
                                echo '<img class="photo" src="'.str_replace(' ', '%20', html_encode(getFullImageURL())).'" alt="'.getImageDesc().'" />';
                            } else {
                                echo '<video class="video" controls src="'.str_replace(' ', '%20', html_encode(getFullImageURL())).'" poster="'.str_replace(' ', '%20', html_encode($_zp_current_image->getCustomImage(1110, null, null, null, null, null, null, true))).'"><a href="'.str_replace(' ', '%20', html_encode(getFullImageURL())).'">Télécharger la vidéo</a></video>';
                            }
                        ?>
                        <?php
                        if (isImagePhoto()) {
                            @call_user_func('printUserSizeSelector');
                        }
                        ?>
                    </div>
                    <div class="social-buttons">
                        <div class="social-buttons-text">Partager&nbsp;: </div>
                        <div class="social">
                            <ul class="rrssb-buttons clearfix">
                              <li class="rrssb-facebook">
                                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode('http://'.$_SERVER['HTTP_HOST'].getImageURL()); ?>" class="popup">
                                  <span class="rrssb-icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 29 29"><path d="M26.4 0H2.6C1.714 0 0 1.715 0 2.6v23.8c0 .884 1.715 2.6 2.6 2.6h12.393V17.988h-3.996v-3.98h3.997v-3.062c0-3.746 2.835-5.97 6.177-5.97 1.6 0 2.444.173 2.845.226v3.792H21.18c-1.817 0-2.156.9-2.156 2.168v2.847h5.045l-.66 3.978h-4.386V29H26.4c.884 0 2.6-1.716 2.6-2.6V2.6c0-.885-1.716-2.6-2.6-2.6z"/></svg></span>
                                  <span class="rrssb-text">Facebook</span>
                                </a>
                              </li>
                            </ul>
                        </div>
                        <div class="social">
                            <ul class="rrssb-buttons clearfix">
                              <li class="rrssb-twitter">
                                <a href="https://twitter.com/intent/tweet?text=<?php echo urlencode('http://'.$_SERVER['HTTP_HOST'].getImageURL()); ?>&amp;via=animafac"
                                class="popup">
                                  <span class="rrssb-icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 28"><path d="M24.253 8.756C24.69 17.08 18.297 24.182 9.97 24.62a15.093 15.093 0 0 1-8.86-2.32c2.702.18 5.375-.648 7.507-2.32a5.417 5.417 0 0 1-4.49-3.64c.802.13 1.62.077 2.4-.154a5.416 5.416 0 0 1-4.412-5.11 5.43 5.43 0 0 0 2.168.387A5.416 5.416 0 0 1 2.89 4.498a15.09 15.09 0 0 0 10.913 5.573 5.185 5.185 0 0 1 3.434-6.48 5.18 5.18 0 0 1 5.546 1.682 9.076 9.076 0 0 0 3.33-1.317 5.038 5.038 0 0 1-2.4 2.942 9.068 9.068 0 0 0 3.02-.85 5.05 5.05 0 0 1-2.48 2.71z"/></svg></span>
                                  <span class="rrssb-text">Twitter</span>
                                </a>
                              </li>
                            </ul>
                        </div>
                    </div>
                    <div id="narrow">
                        <?php printImageDesc(); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        zp_apply_filter('theme_body_close');
        ?>
    </body>
</html>
