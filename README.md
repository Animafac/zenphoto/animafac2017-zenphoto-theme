# Animafac 2017 Zenphoto theme

[Zenphoto](https://www.zenphoto.org/) theme used on [2017.animafac.net](https://2017.animafac.net/Engageons-2017/)

## Setup

The theme needs to be installed in the `themes/` directory of Zenphoto.

You then need to use [Yarn](https://yarnpkg.com/fr/) and [Bower](https://bower.io/) to install the dependencies:

```bash
yarn install
yarn bower install
```

You also need to enable the theme in Zenphoto settings.
