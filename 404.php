<?php
// force UTF-8 Ø

if (!defined('WEBPATH')) {
    die();
}
http_response_code(404);
?>
<!DOCTYPE HTML>
<head>
    <title>Page introuvable</title>
    <?php zp_apply_filter('theme_head'); ?>
    <link rel="stylesheet" href="<?php echo pathurlencode($zenCSS); ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo pathurlencode(dirname(dirname($zenCSS))); ?>/common.css" type="text/css" />
    <?php include_once __DIR__.'/inc/head.php'; ?>
</head>
<body class="single">
    <div class="wrap container">
        <div class="header-fiche"><h1>Page introuvable</h1></div>
        <div class="main">
            <div id="main">
                <div class="fiche-content-full">
                    Cette page n'existe pas.
                </div>
            </div>
    </div>
    <?php
    include __DIR__.'/inc/footer.php';
    ?>
</body>
