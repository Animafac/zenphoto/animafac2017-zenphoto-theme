/*global $*/
/*jslint browser: true*/
$(document).ready(
    function () {
        'use strict';
        $('.lightbox').magnificPopup({
            type: 'iframe',
            gallery: {
                enabled: true
            }
        });
    }
);
