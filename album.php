<?php
// force UTF-8 Ø

if (!defined('WEBPATH')) {
    die();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="<?php echo LOCAL_CHARSET; ?>">
        <?php zp_apply_filter('theme_head'); ?>
        <?php printHeadTitle(' - '); ?>
        <link rel="stylesheet" href="<?php echo pathurlencode($zenCSS); ?>" type="text/css" />
        <link rel="stylesheet" href="<?php echo pathurlencode(dirname(dirname($zenCSS))); ?>/common.css" type="text/css" />
        <?php
        if (class_exists('RSS')) {
            printRSSHeaderLink('Album', getAlbumTitle());
        }
        ?>
        <?php include_once __DIR__.'/inc/head.php'; ?>
    </head>
    <body class="single home-album">
        <?php zp_apply_filter('theme_body_open'); ?>
        <div class="wrap container">
            <div class="main fiche-content-full">
                <div class="header-fiche"><h1><img alt="<?php printAlbumTitle(); ?>" src="<?php echo pathurlencode(dirname(dirname($zenCSS))); ?>/images/Logo%20Engageons%202017-noir.png" /></h1></div>
                <div id="main">
                    <div class="intro">
                        <h2>Animafac donne la parole aux jeunes engagé.e.s</h2>
                        <p>Qu’est-ce que l’engagement en 2017 ? Quelles sont les difficultés rencontrées par les jeunes qui s’investissent pour faire bouger les lignes ? Quelles sont leurs idées pour favoriser l’engagement ? La jeunesse engagée répond à ces questions au travers de témoignages qui sont autant de pistes de réflexion pour une société plus solidaire et plus engagée.<br/>
                        #Engageons2017</p>
                        <p>Vous souhaitez partager votre expérience ? Ajoutez directement votre témoignage sur <a href="https://forms.animafac.net/index.php/939952?lang=fr" title="Envoyer son témoignage" target="_blank">cette page</a> ou envoyez vos vidéos (100 Mo max.) à <a href="mailto:engageons2017@animafac.net">engageons2017@animafac.net</a>.</p>
                        <p><i>Retrouvez nos idées pour une société de l’engagement en cliquant <a href="https://www.animafac.net/minisite/engageons-2017/synthese-des-propositions/" title="Engageons 2017 : nos idées pour une société de l’engagement !">ici</a>, ainsi que notre argumentaire complet par <a title="Engageons 2017 : l'argumentaire" href="https://www.animafac.net/minisite/engageons-2017/societe-de-lengagement/">ici</a>.</i></p>
                    </div>
                    <div class="intro-video">
                        <iframe width="500" height="281" src="https://www.youtube-nocookie.com/embed/rv2604F7EwQ" style="border: none;" allowfullscreen></iframe>
                        <small>Vidéo montée par <a href="http://marionbiancone.wixsite.com/marionbianconeeditor" target="_blank">Marion Biancone</a></small>
                    </div>
                    <br class="clearfloat">
                    <div id="images">
                        <?php
                        if (getOption('Allow_search')) {
                            printSearchForm(null, 'search', 'https://www.animafac.net/wp-content//themes/animafac-wp-theme/assets/img/s.png');
                        }
                        ?>
                        <a href="https://forms.animafac.net/index.php/939952?lang=fr" class="form-link" target="_blank"><span class="dashicons dashicons-admin-comments"></span> Envoyez-nous vos témoignages&nbsp;!</a>
                        <?php while (next_image()) : ?>
                            <div class="image">
                                <div class="imagethumb">
                                    <a class="lightbox" data-mfp-src="<?php echo html_encode(getImageURL()); ?>?iframe=1" href="<?php echo html_encode(getImageURL()); ?>">
                                        <?php printImageThumb(); ?>
                                        <span class="desc"><?php
                                        $title = getImageTitle();
                                        $title = explode(',', $title);
                                        foreach ($title as &$keyword) {
                                            $keyword = trim($keyword);
                                            $keyword = mb_strtoupper(mb_substr($keyword, 0, 1)).mb_substr($keyword, 1);
                                        }
                                        echo implode('<br/>', $title);
                                        ?></span>
                                    </a>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                    <br class="clearfloat">
                    <?php
                    printPageListWithNav("« " . gettext("prev"), gettext("next") . " »");
                    if (function_exists('printAddToFavorites')) {
                        printAddToFavorites($_zp_current_album);
                    }
                    printTags('links', gettext('<strong>Tags:</strong>') . ' ', 'taglist', '');
                    @call_user_func('printGoogleMap');
                    @call_user_func('printSlideShowLink');
                    @call_user_func('printRating');
                    @call_user_func('printCommentForm');
        ?>
                </div>
            </div>
        </div>
        <?php
        zp_apply_filter('theme_body_close');
        ?>
        <script>
            $('#search_input').attr('placeholder', 'Prénom, asso');
        </script>
    </body>
</html>
