<?php
// force UTF-8
if (!defined('WEBPATH')) {
    die();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="<?php echo LOCAL_CHARSET; ?>">
        <?php zp_apply_filter('theme_head'); ?>
        <?php printHeadTitle(); ?>
        <link rel="stylesheet" href="<?php echo pathurlencode($zenCSS); ?>" type="text/css" />
        <link rel="stylesheet" href="<?php echo pathurlencode(dirname(dirname($zenCSS))); ?>/common.css" type="text/css" />
        <?php if (class_exists('RSS')) {
            printRSSHeaderLink('Gallery', gettext('Gallery RSS'));
} ?>
        <?php include_once __DIR__.'/inc/head.php'; ?>
    </head>
    <body class="single">
        <?php
        zp_apply_filter('theme_body_open');
        $total = getNumImages() + getNumAlbums();
        if (!$total) {
            $_zp_current_search->clearSearchWords();
        }
        ?>
        <?php include __DIR__.'/inc/header.php'; ?>
        <div class="wrap container">
            <div class="header-fiche"><h1>Recherche</h1></div>
            <a class="getback" href="<?php echo FULLWEBPATH; ?>">◀ Retourner à la galerie</a>
            <div id="main">
                <div id="padbox">
                    <?php
                    if (($total = getNumImages() + getNumAlbums()) > 0) {
                        if (isset($_REQUEST['date'])) {
                            $searchwords = getSearchDate();
                        } else {
                            $searchwords = getSearchWords();
                        }
                        echo '<p>' . sprintf(gettext('Total matches for <em>%1$s</em>: %2$u'), html_encode($searchwords), $total) . '</p>';
                    }
                    $c = 0;
                    ?>
                    <br class="clearall">
                    <div id="images">
                        <?php
                        if (getOption('Allow_search')) {
                            printSearchForm(null, 'search', 'https://www.animafac.net/wp-content//themes/animafac-wp-theme/assets/img/s.png');
                        }
                        ?>
                        <?php
                        while (next_image()) {
                            $c++;
                            ?>
                            <div class="image">
                                <div class="imagethumb">
                                    <a class="lightbox" data-mfp-src="<?php echo html_encode(getImageURL()); ?>?iframe=1" href="<?php echo html_encode(getImageURL()); ?>">
                                        <?php printImageThumb(); ?>
                                        <span class="desc"><?php
                                        $title = getImageTitle();
                                        $title = explode(',', $title);
                                        foreach ($title as &$keyword) {
                                            $keyword = trim($keyword);
                                            $keyword = mb_strtoupper(mb_substr($keyword, 0, 1)).mb_substr($keyword, 1);
                                        }
                                        echo implode('<br/>', $title);
                                        ?></span>
                                    </a>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        <?php
                        if ($c == 0) {
                            echo "<p>" . gettext("Sorry, no image matches found. Try refining your search.") . "</p>";
                        }
                        ?>
                    </div>
                    <br class="clearall">
                </div>
            </div>
        </div>
        <?php include __DIR__.'/inc/footer.php' ?>
        <?php
        zp_apply_filter('theme_body_close');
        ?>
    </body>
</html>
