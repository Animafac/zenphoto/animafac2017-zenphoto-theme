<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

<link rel="stylesheet" href="https://www.animafac.net//wp-content//themes/animafac-wp-theme/assets/rrssb/css/rrssb.css">
<link rel="stylesheet" href="https://www.animafac.net/wp-content/themes/animafac-wp-theme/assets/css/bootstrap.css">
<link rel="stylesheet" href="https://www.animafac.net/wp-includes/css/dashicons.min.css">
<link rel="stylesheet" href="https://www.animafac.net/wp-content/themes/animafac-wp-theme/assets/css/app.css">
<link rel="stylesheet" href="https://www.animafac.net/wp-content/themes/animafac-wp-theme/assets/css/ql.css">
<link rel="stylesheet" href="https://www.animafac.net/wp-content/themes/animafac-wp-theme/assets/css/responsive.css">

<script type='text/javascript' src='https://www.animafac.net//wp-content//themes/animafac-wp-theme/assets/rrssb/js/rrssb.min.js'></script>

<?php
echo '<script src="'.pathurlencode(dirname(dirname($zenCSS))).
    '/bower_components/magnific-popup/dist/jquery.magnific-popup.min.js"></script>';
echo '<script src="'.pathurlencode(dirname(dirname($zenCSS))).'/scripts/lightbox.js"></script>';

echo '<link rel="stylesheet" href="'.pathurlencode(dirname(dirname($zenCSS))).'/styles/2017.css" />';
echo '<link rel="stylesheet" href="'.pathurlencode(dirname(dirname($zenCSS))).
    '/bower_components/magnific-popup/dist/magnific-popup.css" />';
echo '<link rel="icon" href="'.pathurlencode(dirname(dirname($zenCSS))).'/images/favicon.svg" />';
?>

<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  // tracker methods like "setCustomDimension" should be called before "trackPageView"
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//analytics.animafac.net/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', '10']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<!-- End Piwik Code -->
